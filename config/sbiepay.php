<?php
class sbiepay
{
    public $merchant_id;
    public $encryption_key;
    public $aggregator_id;
    public $account_identifier;
    public $success_url;
    public $fail_url;

    const DEFAULT_BASE_URL = 'https://test.sbiepay.sbi/secure/AggregatorHostedListener?';

    public function __construct()
    {
        $this->merchant_id              =    env('SBIEPAY_MERCHANT_ID');
        $this->encryption_key           =    env('SBIEPAY_MERCHANT_KEY');
        $this->aggregator_id            =    env('SBIEPAY_AGGREGATOR_ID');
        $this->account_identifier       =   env('SBIEPAY_ACCOUNT_IDENTIFIER');
        $this->success_url               =    'http://localhost/laravel_8/public/success';
        $this->fail_url               =    'http://localhost/laravel_8/public/fail';
    }

    public function getPaymentUrl($amount, $reference_no, $optionalField)
    {
        $mandatoryField   =    $this->getMandatoryField($amount, $reference_no);
        $optionalField    =    $this->getOptionalField($optionalField);
        $amount           =    $this->getAmount($amount);
        $reference_no     =    $this->getReferenceNo($reference_no);

        $paymentUrl = $this->generatePaymentUrl($mandatoryField, $optionalField, $amount, $reference_no);
        return $paymentUrl;
    }

    protected function generatePaymentUrl($mandatoryField, $optionalField, $amount, $reference_no)
    {
        $encrypteddata = $this->getEncryptValue($this->merchant_id."|DOM|IN|INR|".$amount."|Other|".$this->success_url."|".$this->fail_url."|".$this->aggregator_id."|".$reference_no."|2|NB|".$this->account_identifier."|".$this->account_identifier);
        $encryptedUrl = self::DEFAULT_BASE_URL.$encrypteddata;

        return $encryptedUrl;
    }

    protected function getMandatoryField($amount, $reference_no)
    {
        return $this->getEncryptValue($reference_no.'|'.$this->merchant_id.'|'.$amount);
    }

    // optional field must be seperated with | eg. (20|20|20|20)
    protected function getOptionalField($optionalField=null)
    {
        if (!is_null($optionalField)) {
            return $this->getEncryptValue($optionalField);
        }
        return null;
    }

    protected function getAmount($amount)
    {
        return $this->getEncryptValue($amount);
    }

    protected function getReturnUrl()
    {
        return $this->getEncryptValue($this->return_url);
    }

    protected function getReferenceNo($reference_no)
    {
        return $this->getEncryptValue($reference_no);
    }

    protected function getSubMerchantId()
    {
        return $this->getEncryptValue($this->sub_merchant_id);
    }

    protected function getPaymode()
    {
        return $this->getEncryptValue($this->paymode);
    }

    // use @ to avoid php warning php

    protected function getEncryptValue($data)
    {
        // Generate an initialization vector
        // $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 128 encryption in ecb mode using our encryption key and initialization vector.
        $iv=substr($this->encryption_key, 0, 16);
        $encrypted = openssl_encrypt($data, 'aes-128-cbc', $this->encryption_key, OPENSSL_RAW_DATA,$iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted);
    }
}