<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();

        Admin::create([
            'username' => 'SuperAdmin',
            'email' => 'superadmin@admin.com',
            'password' => Hash::make('123456'),
            'status' => Admin::ACTIVE,
            'token' => \Str::random(80)
        ]);
    }
}
