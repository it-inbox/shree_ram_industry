<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Seeder;

class CountriesSedder extends Seeder
{
    public function run()
    {
        Country::truncate();

        $data = json_decode(file_get_contents(public_path() . "/countries.json"), true);
        $countries = (object)$data;

        foreach ($countries as $key => $country){
            Country::create([
                'name' => $country['name'],
                'dial_code' => '+'.$country['phone_code'],
                'code' => $country['iso2'],
            ]);
        }

        State::truncate();

        $stateData = json_decode(file_get_contents(public_path() . "/states.json"), true);
        $states = (object)$stateData;

        foreach ($states as $key => $state){
            State::create([
                'country_id' => $state['country_id'],
                'country_code' => $state['country_code'],
                'state_code' => $state['state_code'],
                'name' => $state['name'],
                'slug' =>\Str::slug($state['name'],'-')
            ]);
        }

        // City::truncate();

        // $cityData = json_decode(file_get_contents(public_path() . "/cities.json"), true);
        // $cities = (object)$cityData;

        // foreach ($cities as $key => $city){
        //     City::create([
        //         'country_id' => $city['country_id'],
        //         'state_id' => $city['state_id'],
        //         'state_code' => $city['state_code'],
        //         'name' => $city['name']
        //     ]);
        // }
    }
}
