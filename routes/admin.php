<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\Admin\MasterController;
use App\Http\Controllers\Admin\CmsController;
use App\Http\Controllers\Admin\Client_logoController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\IndustryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SupportController;
use App\Http\Controllers\Admin\BookLangaugeController;



Route::prefix('admin')->group(function () {
    Route::get('/',function(){
        return redirect()->route('admin-login');
    });
    Route::get('login',[LoginController::class,'index'])->name('admin-login');
    Route::post('auth',[LoginController::class,'Auth'])->name('admin-auth');

    Route::group(['middleware' => 'admin.auth'],function(){
        Route::get('dashboard',[DashboardController::class,'index'])->name('admin-dashboard');

        Route::group(['prefix' => 'manager'], function () {
            Route::get('profile', [LoginController::class,'profile'])->name('admin-manager-profile');
            Route::post('profile', [LoginController::class,'profile'])->name('admin-manager-profile');
        });

        Route::group(['prefix' => 'user'],function (){
            Route::get('/',[UserController::class,'index'])->name('user-list-view');
            Route::get('create',[UserController::class,'create'])->name('user-list-create');
            Route::post('store',[UserController::class,'store'])->name('user-list-store');
            Route::get('edit_user/{id}',[UserController::class,'edit'])->name('edit_user');
            Route::post('user-update',[UserController::class,'update'])->name('user-update');
            Route::get('delete_user/{id}',[UserController::class,'deleteUser'])->name('delete_user');

            Route::get('admin-user-account-access/{id}',[LoginController::class,'adminUserAccountAccess'])->name('admin-user-account-access');
        });

        Route::group(['prefix' => 'settings'],function(){
            Route::get('/',[SettingController::class,'index'])->name('admin-settings');
            Route::post('update',[SettingController::class,'update'])->name('admin-settings-update');
        });


        //cms 
        Route::group(['prefix' => 'cms'], function () {
            Route::get('/', [CmsController::class, 'index'])->name('admin-cms'); 
            Route::get('edit/{id}',[CmsController::class,'edit'])->name('admin-cms-edit');
            Route::post('update',[CmsController::class,'update'])->name('admin-cms-update');    
        });

        //Client Logo
        Route::group(['prefix' => 'client-logo'], function () {
            Route::get('/', [Client_logoController::class, 'index'])->name('admin-client-logo'); 
            Route::get('create',[Client_logoController::class,'create'])->name('admin-client-logo-create');
            Route::post('store',[Client_logoController::class,'store'])->name('admin-client-logo-store');
            Route::get('edit/{id}',[Client_logoController::class,'edit'])->name('admin-client-logo-edit');
            Route::post('update',[Client_logoController::class,'update'])->name('admin-client-logo-update');

            // Route::get('edit/{id}',[CmsController::class,'edit'])->name('admin-cms-edit');
            // Route::post('update',[CmsController::class,'update'])->name('admin-cms-update');    
        });

        Route::group(['prefix' =>'currencies'],function(){
            Route::get('/',[MasterController::class,'currencyIndex'])->name('currencies_list_view');
            Route::get('create',[MasterController::class,'currencyCreate'])->name('currencies_create');
            Route::post('create',[MasterController::class,'currencyCreate'])->name('currencies_create');
            Route::get('update/{id}',[MasterController::class,'currencyUpdate'])->name('currencies_update');
            Route::post('update/{id}',[MasterController::class,'currencyUpdate'])->name('currencies_update');
        });
        Route::group(['prefix' =>'book_langauage'],function(){
            Route::get('/',[BookLangaugeController::class,'index'])->name('book_language_view');
            Route::get('create',[BookLangaugeController::class,'create'])->name('languages_create');
            Route::post('create',[BookLangaugeController::class,'create'])->name('languages_create');
            Route::get('update/{id}',[BookLangaugeController::class,'update'])->name('languages_update');
            Route::post('update/{id}',[BookLangaugeController::class,'update'])->name('languages_update');
        });

        Route::group(['prefix' => 'services'],function(){
            Route::get('/',[ServiceController::class,'index'])->name('admin-service-view');
            Route::get('create',[ServiceController::class,'create'])->name('admin-service-create');
            Route::post('store',[ServiceController::class,'store'])->name('admin-service-store');
            Route::get('update/{id}',[ServiceController::class,'edit'])->name('admin-service-edit');
            Route::post('update',[ServiceController::class,'update'])->name('admin-service-update');
        });

        Route::group(['prefix' => 'industries'],function(){
            Route::get('/',[IndustryController::class,'index'])->name('admin-industry-view');
            Route::get('create',[IndustryController::class,'create'])->name('admin-industry-create');
            Route::post('store',[IndustryController::class,'store'])->name('admin-industry-store');
            Route::get('edit/{id}',[IndustryController::class,'edit'])->name('admin-industry-edit');
            Route::post('update',[IndustryController::class,'update'])->name('admin-industry-update');
        });

        Route::group(['prefix' => 'products'],function(){
            Route::get('/',[ProductController::class,'index'])->name('admin-product-view');
            Route::get('create',[ProductController::class,'create'])->name('admin-product-create');
            Route::post('store',[ProductController::class,'store'])->name('admin-product-store');
            Route::get('edit/{id}',[ProductController::class,'edit'])->name('admin-product-edit');
            Route::post('update',[ProductController::class,'update'])->name('admin-product-update');
        });

        Route::group(['prefix' => 'support'], function () {
            Route::get('website-inquiry',[SupportController::class,'website'])->name('admin-website-support');
        });
    });
    Route::get('logout',[LoginController::class,'logout'])->name('admin-logout');
});

 