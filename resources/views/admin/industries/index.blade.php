@extends('admin.template.layout')
@section('title','Industries')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Industries:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('admin-industry-create')  }}" class="btn btn-primary pull-right">Create</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($industries as $index => $industry)
                        <tr>
                            <td>{{ $index + 1  }}</td>
                            <td><img src="{{ asset($industry->image)  }}" alt="{{ $industry->title  }}" width="100px"></td>
                            <td>{{ $industry->title  }}</td>
                            <td>
                                @if($industry->status === \App\Models\Industry::ACTIVE)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">In-active</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-industry-edit',[$industry->id])  }}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection