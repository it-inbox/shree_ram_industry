@extends('admin.template.layout')
@section('title', 'Client Logo')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Client Logo:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
        <div class="col-md-6 offset-md-3 offset-lg-3">
        <div class="card">
            <div class="card-body">
                 <form action="{{ route('admin-client-logo-store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                   
                    <div class="form-group form-group-default">
                        <label for="title">Title:</label>
                        <input type="text" class="form-control" id="title" name="title" value="" required>
                    </div>


                <div class="form-group form-group-default">
                    <label for="logo">Logo:</label>
                    <input type="file" class="form-control" id="logo" name="logo" required>
                </div>


                
                <div class="form-group">
    <label for="status">Status</label>
    <div class="form-check form-check-inline">
        <input type="radio" name="status" id="radioInline1" value="1">
        <label for="radioInline1">
            Active
        </label>
    </div> <div class="form-check form-check-inline complete">
        <input type="radio" name="status" id="radioDisabled2" value="2">
        <label for="radioDisabled2">
            In-active
        </label>
    </div>
</div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>    
        </div>
        
        </div>
    </div>
    </div>

@stop
    