@extends('admin.template.layout')
@section('title', 'Client Logo')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Client Logo:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <input id="myInput" type="text" class="form-control" placeholder="Search by table..."
                            onkeyup="question()">
                    </div>
                    <div class="col-md-2">
                    <a href="{{ route('admin-client-logo-create') }}" class="btn btn-success">Create</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Logo</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($clients as $index => $client)
                            <tr>
                                <td>{{  $index + 1  }}</td>
                                <td>{{ $client->title }}</td>
                                <td>
                                    <img src="{{ asset('storage/'.$client->logo) }}" alt="{{ $client->title }}" width="50px">
                                </td>
                                <td>
                                    @if($client->status == \App\Models\Logo::ACTIVE)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">In-active</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-client-logo-edit', ['id' => $client->id])}}" class="btn btn-warning" title="Edit Page"><i class="fa fa-pencil"></i></a>
                                </td>



                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    @stop