@extends('admin.template.layout')
@section('title','Website Inquiry')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Website-inquiry:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Message</th>
                        <th>Created On</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($inquiries as $index => $inquiry)
                        <tr>
                            <td>{{ $index + 1  }}</td>
                            <td>{{ $inquiry->name }}</td>
                            <td>{{ $inquiry->email }}</td>
                            <td>{{ $inquiry->contact }}</td>
                            <td><div class="message-box">{{ $inquiry->message }}</div></td>
                            <td>{{ $inquiry->created_at->format('d-m-Y')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection