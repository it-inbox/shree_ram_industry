@extends('admin.template.layout')
@section('title','Services')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Services:admin-service-view,Update:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin-service-update')  }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{ $service->id  }}" name="id">
                            <div class="form-group form-group-default">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" value="{{ $service->title  }}">
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">Description</label>
                                <textarea name="description" class="form-control" id="description" rows="10">{{ $service->description  }}</textarea>
                            </div>
                            <div class="profile-photo">
                                <label>Service Image</label>
                                <div class="dz-default dlab-message upload-img mb-3">
                                    <div class="fallback">
                                        <input name="service_image" type="file" id="service_image">
                                        <img class="img-circle profile_img" src="" alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="current_photo text-center">
                                <label>Current</label>
                                <img src="{{ asset($service->image)  }}" class="img-circle" alt="">
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="status" id="radioInline1" value="1" {{ $service->status == \App\Models\Service::ACTIVE ? 'checked' : ''  }}>
                                    <label for="radioInline1">
                                        Active
                                    </label>
                                </div> <div class="form-check form-check-inline complete">
                                    <input type="radio" name="status" id="radioDisabled2" value="2" {{ $service->status == \App\Models\Service::INACTIVE ? 'checked' : ''  }}>
                                    <label for="radioDisabled2">
                                        In-active
                                    </label>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#service_image').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    var reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            })
        });
    </script>
@endsection