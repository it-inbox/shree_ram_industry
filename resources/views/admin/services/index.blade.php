@extends('admin.template.layout')
@section('title','Services')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Services:active)
    <div class="container-fluid container-fixed-lg">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('admin-service-create')  }}" class="btn btn-primary pull-right">Create</a>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $index => $service)
                        <tr>
                            <td>{{ $index + 1  }}</td>
                            <td><img src="{{ asset($service->image)  }}" alt="{{ $service->title  }}" width="100px"></td>
                            <td>{{ $service->title  }}</td>
                            <td>
                                @if($service->status === \App\Models\Service::ACTIVE)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">In-active</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-service-edit',[$service->id])  }}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection