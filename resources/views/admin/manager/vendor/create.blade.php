@extends('admin.template.layout')
@section('title','Create')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Vendors:admin-manage-vendors,Create:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin-manage-vendors.store')  }}" method="post">
                            @csrf
                            <div class="form-group form-group-default">
                                <label for="">Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Vendor Name">
                                @error('name')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">URL</label>
                                <input type="text" name="url" class="form-control" placeholder="Enter Vendor URL">
                                @error('url')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">Secret Key</label>
                                <input type="text" name="secret_key" class="form-control" placeholder="Enter Secret Key">
                                @error('secret_key')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">Client Key</label>
                                <input type="text" name="client_key" class="form-control" placeholder="Enter Client Key">
                                @error('client_key')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">UserName</label>
                                <input type="text" name="username" class="form-control" placeholder="Enter UserName">
                                @error('username')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">Password</label>
                                <input type="text" name="password" class="form-control" placeholder="Enter Password">
                                @error('password')
                                <div class="error text-danger mt-3 text-left font-weight-bolder">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection