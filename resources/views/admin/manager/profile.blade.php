@extends('admin.template.layout')
@section('title', 'Admin Profile')
@section('page-content')
    <div class="content">
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
                <div class="inner">
                    <ol class="breadcrumb sm-p-b-5">
                        <li class="breadcrumb-item"><a href="{{ route('admin-dashboard')  }}">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="container-fluid container-fixed-lg">
            <div class="row">
                <div class="col-md-6 offset-md-3 offset-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('admin-manager-profile')  }}" method="post" role="form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group form-group-default">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ Session::get('admin')->username }}" required autocomplete="off">
                                </div>
                                <div class="form-group form-group-default">
                                    <label>Email</label>
                                    <input type="text" class="form-control" value="{{ Session::get('admin')->email }}" disabled autocomplete="off">
                                </div>
                                <div class="form-group form-group-default">
                                    <label>Mobile</label>
                                    <input type="number" class="form-control" name="mobile" value="{{ Session::get('admin')->mobile }}" required autocomplete="off">
                                </div>
                                <div class="form-group form-group-default">
                                    <label>Password</label>
                                    <div class="controls input-group">
                                        <input type="password" class="form-control passwordInput" name="password" placeholder="Credentials" required>
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="showPassword(this)">
                                                <i class="fa fa-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-danger">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('page-js')
    <script>
        function showPassword(element){
            var passwordElement = $(element).parent().parent().find('input.passwordInput');

            var inputType = passwordElement.attr('type');

            if (inputType === 'password') {
                $(element).html('<i class="fa fa-eye"></i>');
                passwordElement.attr('type', 'text');
            }
            else {
                $(element).html('<i class="fa fa-eye-slash"></i>');
                passwordElement.attr('type', 'password');
            }
        }
    </script>
@endsection