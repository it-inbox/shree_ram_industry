@extends('admin.template.layout')
@section('title', 'CMS')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,CMS:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <input id="myInput" type="text" class="form-control" placeholder="Search by table..."
                            onkeyup="question()">
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Page Name</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pagesNames as $page)
                            <tr>
                                <td>{{ $page->id }}</td>
                                <td>{{ $page->page_name }}</td>
                                <td> <a href="{{ route('admin-cms-edit', ['id' => $page->id]) }}" class="btn btn-warning" title="Edit Page">
    <i class="fa fa-pencil"></i>
</a></td>
                                <!-- <td> <a href="{{ route('admin-cms-edit', ['id' => $page->id]) }}" class="btn btn-warning" title="Edit Page"> <i class="fa fa-pencil"></i> </a> </td> -->
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop




