@php
    $request = request()->segments();
@endphp
<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-header text-center">
        <img src="" alt="logo" class="brand" data-src="" data-src-retina="{{ asset('assets/img/logo_2x.png')  }}" width="100" height="50">
    </div>
    <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="m-t-10 {{ end($request) == 'dashboard' ? 'active': ''  }}">
                <a href="{{ route('admin-dashboard')  }}" class="detailed">
                    <span class="title">Dashboard</span>
                </a>
                <span class="icon-thumbnail"><i data-feather="home"></i></span>
            </li>
            {{--<li class="{{ end($request) == 'user' ? 'active': ''  }}">--}}
                {{--<a href="{{ route('user-list-view')  }}"><span class="title">Users</span></a>--}}
                {{--<span class="icon-thumbnail"><i data-feather="user"></i></span>--}}
            {{--</li>--}}
            <li class="{{ end($request) == 'settings' ? 'active': ''  }}">
                <a href="{{ route('admin-settings')  }}"><span class="title">Settings</span></a>
                <span class="icon-thumbnail"><i data-feather="settings"></i></span>
            </li>
            <li class="{{ end($request) == 'cms' ? 'active': ''  }}">
                <a href="{{ route('admin-cms')  }}"><span class="title">CMS Pages</span></a>
                <span class="icon-thumbnail"><i data-feather="list"></i></span>
            </li>
            <li class="{{ end($request) == 'client-logo' ? 'active': ''  }}">
                <a href="{{ route('admin-client-logo')  }}"><span class="title">Our Clients</span></a>
                <span class="icon-thumbnail"><i data-feather="users"></i></span>
            </li>
            <li class="{{ end($request) == 'services' ? 'active': ''  }}">
                <a href="{{ route('admin-service-view')  }}"><span class="title">Services</span></a>
                <span class="icon-thumbnail"><i data-feather="feather"></i></span>
            </li>
            <li class="{{ end($request) == 'industries' ? 'active': ''  }}">
                <a href="{{ route('admin-industry-view')  }}"><span class="title">Industries</span></a>
                <span class="icon-thumbnail"><i data-feather="wind"></i></span>
            </li>
            <li class="{{ end($request) == 'products' ? 'active': ''  }}">
                <a href="{{ route('admin-product-view')  }}"><span class="title">Products</span></a>
                <span class="icon-thumbnail"><i data-feather="grid"></i></span>
            </li>
            <li>
                <a href="javascript:void(0)"><span class="title">Support</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i data-feather="life-buoy"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admin-website-support') }}">Website Inquiry</a>
                        <span class="icon-thumbnail">W</span>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>