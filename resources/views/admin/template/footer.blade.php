<div class=" container-fluid  container-fixed-lg footer">
    <div class="copyright sm-text-center">
        <p class="small-text no-margin pull-left sm-pull-reset">
            ©{{ date('Y')  }} All Rights Reserved.
            <span class="hint-text m-l-15">{{ env('APP_NAME')  }}</span>
        </p>
        <div class="clearfix"></div>
    </div>
</div>