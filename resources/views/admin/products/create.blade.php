@extends('admin.template.layout')
@section('title','Products')
@section('page-content')
    @breadcrumb(Dashboard:admin-dashboard,Products:admin-product-view,Create:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin-product-store')  }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group form-group-default">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title">
                            </div>
                            <div class="form-group form-group-default">
                                <label for="">Description</label>
                                <textarea name="description" class="form-control" id="description" rows="10"></textarea>
                            </div>
                            <div class="profile-photo">
                                <label>Product Image</label>
                                <div class="dz-default dlab-message upload-img mb-3">
                                    <div class="fallback">
                                        <input name="product_image" type="file" id="product_image">
                                        <img class="img-circle profile_img" src="" alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-js')
    <script>
        $(document).ready(function(){
            $('#product_image').change(function (e) {
                e.preventDefault();
                const file = this.files[0];
                if (file){
                    var reader = new FileReader();
                    reader.onload = function(event){
                        $('.profile_img').attr('src', event.target.result);
                    };
                    reader.readAsDataURL(file);
                }
            })
        });
    </script>
@endsection
