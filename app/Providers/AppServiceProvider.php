<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('breadcrumb', function ($expression) {

            $breadcrumbs = explode(',',str_replace(['(',')'], '', $expression));

            $breadcrumbs = collect($breadcrumbs)->map(function ($breadcrumb) {

                $single = explode(':', $breadcrumb);

                if ($single[1] != 'active')
                    return '<li class="breadcrumb-item"><a href="' . route($single[1]) . '">' . $single[0] . '</a></li>';
                else
                    return '<li class="breadcrumb-item active">' . $single[0] . '</li>';
            })->toArray();


            return '<div class="" data-pages="parallax">
                        <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
                            <div class="inner"><ol class="breadcrumb"> ' . implode("", $breadcrumbs) . ' </ol></div>
                        </div>
                    </div>';

        });
    }
}
