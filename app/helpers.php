<?php

if(!function_exists('convertDateToAge')){
    function convertDateToAge($date){
        return \Carbon\Carbon::parse($date)->age;
    }
}