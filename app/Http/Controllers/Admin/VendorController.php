<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    public function index(){
        $vendors = Vendor::orderBy('name','asc')->get();

        return view('admin.manager.vendor.index',[
            'vendors' => $vendors
        ]);
    }

    public function create(){
        return view('admin.manager.vendor.create');
    }

    public function store(Request $request){
        if($request->isMethod('post')){

            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'url' => 'required',
                'username' => 'required',
                'password' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Vendor::create([
                'name' => $request->name,
                'url' => $request->url,
                'secret_key' => $request->secret_key,
                'client_key' => $request->client_key,
                'username' => $request->username,
                'password' => Hash::make($request->password)
            ]);

            return redirect()->route('admin-manage-vendors')->with('success','Vendor Created Successfully!');
        }
    }

    public function edit($id){
        $vendor = Vendor::whereId($id)->first();

        return view('admin.manager.vendor.update',[
            'vendor' => $vendor
        ]);
    }

    public function update(Request $request){
        $vendor = Vendor::whereId($request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'url' => 'required',
                'username' => 'required',
                'password' => 'required'
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $vendor->name = $request->name;
            $vendor->url = $request->url;
            $vendor->secret_key = $request->secret_key;
            $vendor->client_key = $request->client_key;
            $vendor->username = $request->username;
            $vendor->password = $request->password;
            $vendor->save();

            return redirect()->route('admin-manage-vendors')->with('success','Vendor Detail updated Successfully!');
        }
    }
}
