<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Industry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IndustryController extends Controller
{
    public function index(){
        $industries = Industry::orderBy('id','desc')->get();

        return view('admin.industries.index',[
            'industries' => $industries
        ]);
    }

    public function create(){
        return view('admin.industries.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $image = $request->file('industry_image');
        $imageName = '';
        $destinationPath = '';
        if (isset($image)){
            $destinationPath = '/uploads/industry/';
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path().$destinationPath, $imageName);
        }

        Industry::Create([
            'slug' => \Str::slug($request->title,'-'),
            'title' => $request->title,
            'image' => ($request->industry_image ? $destinationPath.$imageName : ''),
            'description' => $request->description,
            'status' => Industry::ACTIVE
        ]);

        return redirect()->route('admin-industry-view')->with('success','Industry Added Successfully');

    }

    public function edit($id){
        $industry = Industry::where('id',$id)->first();

        return view('admin.industries.update',[
            'industry' => $industry
        ]);
    }

    public function update(Request $request){
        $industry = Industry::where('id',$request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'title' => 'required',
                'description' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if($request->industry_image != '' && $request->industry_image != null){
                $image = $request->file('industry_image');
                if (isset($image)){
                    $destinationPath = '/uploads/industry/';
                    $imageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $imageName);
                    $industry->image = $destinationPath.$imageName;
                }
            }else{
                $imageName = $industry->image;
                $industry->image = $imageName;
            }

            $industry->slug  = \Str::slug($request->title,'-');
            $industry->title  = $request->title;
            $industry->description  = $request->description;
            $industry->status  = $request->status;
            $industry->save();

            return redirect()->route('admin-industry-view')->with('success','Industry Updated Successfully');
        }
    }
}
