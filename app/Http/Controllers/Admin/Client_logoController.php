<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Logo;
use Illuminate\Support\Facades\Validator;

class Client_logoController extends Controller
{
    public function index(){
        $clients = Logo::orderBy('id','desc')->get();
       return view('admin.client_logo.index',[
        'clients' => $clients
       ]);
    }

    public function create(){
        return view('admin.client_logo.add');
     }

     
     public function store(Request $request)
     {
        // dd($request->all());
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $fileName =  rand().time().'.'.$file->getClientOriginalExtension();
            $filePath = $file->storeAs('logos', $fileName, 'public');
            $validatedData['logo'] = $filePath;
        } else {
            return redirect()->back()->with('error', 'Please select a logo file.');
        }
        $logo = Logo::create($validatedData);
        return redirect()->route('admin-client-logo')->with('success', 'Logo created successfully');
    }


    public function edit($id)
    {
            $clients = Logo::find($id); 

            return view('admin.client_logo.edit', ['clients' => $clients]);
    }




    public function update(Request $request)
{
    $validatedData = $request->validate([
        'title' => 'required|string|max:255',
        'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Make logo field nullable for cases where you don't want to update the logo
        'status' => 'required|in:1,2',
    ]);

    $logo = Logo::find($request->id);

    if (!$logo) {
        return redirect()->route('admin-client-logo')->with('error', 'Logo not found');
    }

    $logo->title = $validatedData['title'];
    $logo->status = $validatedData['status'];

    if ($request->hasFile('logo')) {
        $file = $request->file('logo');
        $fileName = rand() . time() . '.' . $file->getClientOriginalExtension();
        $filePath = $file->storeAs('logos', $fileName, 'public');
        $logo->logo = $filePath;
    }

    $logo->save();

    return redirect()->route('admin-client-logo')->with('success', 'Logo updated successfully');
}


}
