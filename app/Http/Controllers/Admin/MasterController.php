<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ChannelPreference;
use App\Models\Currency;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Activitylog\Models\Activity;

class MasterController extends Controller
{
    public function currencyIndex(){
        $currencies = Currency::orderBy('id','desc')->get();

        return view('admin.master.currency.index',[
            'currencies' => $currencies
        ]);
    }

    public function currencyCreate(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Currency::create([
                'name' => $request->name,
                'full_name' => ($request->full_name ? $request->full_name : ''),
                'slug' =>  \Str::slug($request->name,'-'),
                'status' => Currency::ACTIVE
            ]);

            
            $currencyActivity = Activity::all()->last();
            return redirect()->route('currencies_list_view')->with(['success' => 'Currency Added Successfully!']);
        }
       
        return view('admin.master.currency.create');
    }

    public function currencyUpdate(Request $request,$id){
        $currency = Currency::where('id',$id)->first();
        $histories = Activity::where('subject_id', $id)->orderBy('id', 'desc')->get();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $currency->name =  $request->name;
            $currency->full_name = ($request->full_name ? $request->full_name : '');
            $currency->slug =  \Str::slug($request->name,'-');
            $currency->status = $request->status;
            $currency->save();

            $activity = Activity::all()->last();

            return redirect()->route('currencies_list_view')->with(['success' => 'Currency updated Successfully!']);
        }
        
        return view('admin.master.currency.update',[
            'currency' => $currency,
            'histories' => $histories
        ]);
    }


    public function languageIndex(){
        $languages = Language::orderBy('id','desc')->get();

        return view('admin.master.language.index',[
            'languages' => $languages
        ]);
    }

    public function languageCreate(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            Language::create([
                'name' => $request->name,
                'slug' =>  \Str::slug($request->name,'-'),
                'status' => Language::ACTIVE
            ]);
            $languageActivity = Activity::all()->last();
            return redirect()->route('languages_list_view')->with(['success' => 'Language Added Successfully!']);
        }
        return view('admin.master.language.create');
    }

    public function languageUpdate(Request $request,$id){
        $language = Language::where('id',$id)->first();
        $histories = Activity::where('subject_id', $id)->orderBy('id', 'desc')->get();
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $language->name =  $request->name;
            $language->slug =  \Str::slug($request->name,'-');
            $language->status = $request->status;
            $language->save();

            $activity = Activity::all()->last();
            return redirect()->route('languages_list_view')->with(['success' => 'Language updated Successfully!']);
        }

        return view('admin.master.language.update',[
            'language' => $language,
            'histories' => $histories
        ]);
    }

    public function channelPreferencesIndex(){
        $channel_preferences = ChannelPreference::orderBy('id','desc')->get();

        return view('admin.master.channel_preferences.index',[
            'channel_preferences' => $channel_preferences
        ]);
    }

    public function channelPreferencesCreate(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            ChannelPreference::create([
                'name' => $request->name,
                'slug' =>  \Str::slug($request->name,'-'),
                'status' => ChannelPreference::ACTIVE
            ]);
            $channelActivity = Activity::all()->last();
            return redirect()->route('channel_preferences_list_view')->with(['success' => 'Channel Preferences Added Successfully!']);
        }
        return view('admin.master.channel_preferences.create');
    }

    public function channelPreferencesUpdate(Request $request,$id){
        $channel_preference = ChannelPreference::where('id',$id)->first();
        $histories = Activity::where('subject_id', $id)->orderBy('id', 'desc')->get();
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $channel_preference->name =  $request->name;
            $channel_preference->slug =  \Str::slug($request->name,'-');
            $channel_preference->status = $request->status;
            $channel_preference->save();

            $activity = Activity::all()->last();
            return redirect()->route('channel_preferences_list_view')->with(['success' => 'Channel Preferences updated Successfully!']);
        }

        return view('admin.master.channel_preferences.update',[
            'channel_preference' => $channel_preference,
            'histories' => $histories
        ]);
    }
}
