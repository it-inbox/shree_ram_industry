<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Package;
use Illuminate\Support\Facades\Validator;
use Spatie\Activitylog\Models\Activity;


class PackageController extends Controller
{
    public function index()
    {
        $packages = Package::orderBy('id','desc')->paginate(10);


        return view('admin.packages.index',[
            'packages' => $packages
        ]);
    }

    public function create()
    {
        $currencies = Currency::where('status',Currency::ACTIVE)->get();

        return view('admin.packages.create',[
            'currencies' => $currencies
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'description' => 'required|string|max:250',
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Package::create([
            'name' => $request->name,
            'description' => $request->description,
            'currency' => $request->currency,
            'amount' => $request->amount,
            'status' => Package::ACTIVE,
            'created_at' => Carbon::now()->format('Y-m-d h:i s'),
            'updated_at' => Carbon::now()->format('Y-m-d h:i s'),
        ]);
        $packageActivity = Activity::all()->last();  
        return redirect()->route('packages-list-view')->with('success', 'Package added successfully');
    }

    public function view($id)
    {
        $package = Package::where('id',$id)->first();
        $currencies = Currency::where('status',Currency::ACTIVE)->get();

        return view('admin.packages.view',[
            'package' => $package,
            'currencies' => $currencies
        ]);
    }

    public function edit($id)
    {
        $package = Package::where('id',$id)->first();
        $currencies = Currency::where('status',Currency::ACTIVE)->get();
        $histories = Activity::where('subject_id',$id)->orderBy('id','desc')->get();
        return view('admin.packages.update',[
            'package' => $package,
            'currencies' => $currencies,
            'histories' => $histories
        ]);
    }

    public function update(Request $request)
    {
        $package = Package::where('id',$request->id)->first();

        if($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'description' => 'required|string|max:250',
                'amount' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $package->name = $request->name;
            $package->description = $request->description;
            $package->currency = $request->currency;
            $package->amount = $request->amount;
            $package->status = ($request->status == '1' ? Package::ACTIVE : Package::INACTIVE);
            $package->updated_at = Carbon::now();
            $package->save();

            $activity = Activity::all()->last();
            return redirect()->route('packages-list-view')->with('success', 'Package updated successfully');
        }
    }
}
