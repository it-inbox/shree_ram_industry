<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pages_name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CmsController extends Controller
{
    public function index()
    {
        $pagesNames = Pages_name::all();
        return view('admin.cms.index', compact('pagesNames'));
    }

    public function edit($id)
    {
    $pagesNames = Pages_name::findOrFail($id);

    return view('admin.cms.update', compact('pagesNames'));
    }

    

    public function update(Request $request){
        $pagesNames = Pages_name::where('id',$request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'title' => 'required',
                'description' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $pagesNames->title =  $request->title;
            $pagesNames->description =  $request->description;
            $pagesNames->status = $request->status;
            $pagesNames->save();

            return redirect()->route('admin-cms')->with(['success' => $pagesNames->title.'detail updated Successfully!']);
        }
        
    }





}






