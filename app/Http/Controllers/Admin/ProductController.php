<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(){
        $products = Product::orderBy('id','desc')->get();

        return view('admin.products.index',[
            'products' => $products
        ]);
    }

    public function create(){
        return view('admin.products.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $image = $request->file('product_image');
        $imageName = '';
        $destinationPath = '';
        if (isset($image)){
            $destinationPath = '/uploads/products/';
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path().$destinationPath, $imageName);
        }

        Product::Create([
            'slug' => \Str::slug($request->title,'-'),
            'title' => $request->title,
            'image' => ($request->product_image ? $destinationPath.$imageName : ''),
            'description' => $request->description,
            'status' => Product::ACTIVE
        ]);

        return redirect()->route('admin-product-view')->with('success','Product Added Successfully');

    }

    public function edit($id){
        $product = Product::where('id',$id)->first();

        return view('admin.products.update',[
            'product' => $product
        ]);
    }

    public function update(Request $request){
        $product = Product::where('id',$request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'title' => 'required',
                'description' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if($request->product_image != '' && $request->product_image != null){
                $image = $request->file('product_image');
                if (isset($image)){
                    $destinationPath = '/uploads/products/';
                    $imageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $imageName);
                    $product->image = $destinationPath.$imageName;
                }
            }else{
                $imageName = $product->image;
                $product->image = $imageName;
            }

            $product->slug  = \Str::slug($request->title,'-');
            $product->title  = $request->title;
            $product->description  = $request->description;
            $product->status  = $request->status;
            $product->save();

            return redirect()->route('admin-product-view')->with('success','Product Updated Successfully');
        }
    }
}
