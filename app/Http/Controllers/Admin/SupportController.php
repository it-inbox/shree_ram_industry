<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactInquiry;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function website(){

        return view('admin.support.website-inquiry',[
            'inquiries' => ContactInquiry::orderBy('id','desc')->get()
        ]);
    }
}
