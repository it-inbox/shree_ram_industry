<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function index(){
    $services = Service::orderBy('id','desc')->get();

    return view('admin.services.index',[
        'services' => $services
    ]);
}

    public function create(){
        return view('admin.services.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $image = $request->file('service_image');
        $imageName = '';
        $destinationPath = '';
        if (isset($image)){
            $destinationPath = '/uploads/services/';
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(public_path().$destinationPath, $imageName);
        }

        Service::create([
            'slug' => \Str::slug($request->title,'-'),
            'title' => $request->title,
            'image' => ($request->service_image ? $destinationPath.$imageName : ''),
            'description' => $request->description,
            'status' => Service::ACTIVE
        ]);

        return redirect()->route('admin-service-view')->with('success','Service Added Successfully');

    }

    public function edit($id){
        $service = Service::where('id',$id)->first();

        return view('admin.services.update',[
            'service' => $service
        ]);
    }

    public function update(Request $request){

        $service = Service::where('id',$request->id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(),[
                'title' => 'required',
                'description' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if($request->service_image != '' && $request->service_image != null){
                $image = $request->file('service_image');
                if (isset($image)){
                    $destinationPath = '/uploads/services/';
                    $imageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $imageName);
                    $service->image = $destinationPath.$imageName;
                }
            }else{
                $imageName = $service->image;
                $service->image = $imageName;
            }

            $service->slug  = \Str::slug($request->title,'-');
            $service->title  = $request->title;
            $service->description  = $request->description;
            $service->status  = $request->status;
            $service->save();

            return redirect()->route('admin-service-view')->with('success','Service Updated Successfully');
        }
    }
}
