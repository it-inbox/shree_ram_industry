<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function index(){
        $countries = Country::pluck('name','id');
        $setting = Setting::orderBy('id','desc')->first();

        return view('admin.settings',[
            'countries' => $countries,
            'setting' => $setting
        ]);
    }

    public function update(Request $request){

        if($request->isMethod('post')){

            $validator = Validator::make([
                'company_name' => 'required',
                'company_email' => 'required|email',
                'company_number' => 'required|max:10',
                'pan_number' => 'required',
                'gst_no' => 'required',
                'company_address' => 'required',
                'pin_code' => 'required|max:6',
                'city' => 'required',
                'state'=> 'required',
                'country' => 'required'
            ],[
                'company_name.required' => 'Company Name Number Is Required',
                'company_email.required' => 'Email Address Is Required',
                'company_number.required' => 'Mobile Number Is Required',
                'pan_number.required' => 'Pan Number Is Required',
                'gst_no.required' => 'GST No Is Required',
                'company_address.required' => 'Company Address Is Required',
                'pin_code.required' => 'Pin Code is Required',
                'state.required' => 'State is Required',
                'country.required' =>'Country is Required'
            ]);

            // dd($request->all());

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $setting = Setting::orderBy('id','desc')->first();
            if(!empty($setting)){

                $logoImage = "";
                $image = $request->file('site_logo');
                $siteImageName = '';
                $destinationPath = '/admin-assets/company/';
                if (isset($image)){
                    $destinationPath = '/admin-assets/company/';
                    $siteImageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $siteImageName);
                    $logoImage = $destinationPath.$siteImageName;
                    $setting->site_logo =  $logoImage;
                }else{
                    $logoImage = $setting->site_logo;
                    $setting->site_logo =  $logoImage;
                }

                $fav_image = $request->file('site_fav_logo');
                $siteFaviconImageName = '';
                if (isset($fav_image)){
                    $destinationPath = '/admin-assets/company/';
                    $siteFaviconImageName = rand().time().'.'.$fav_image->getClientOriginalExtension();
                    $fav_image->move(public_path().$destinationPath, $siteFaviconImageName);
                    $faviconImage = $destinationPath.$siteFaviconImageName;
                }else{
                    $faviconImage = $setting->site_fav_logo;
                }

                $setting->site_fav_logo =  ($faviconImage ? $faviconImage : '');

                $brochure_file = $request->file('brochure');
                $brochureName = '';
                if (isset($brochure_file)){
                    $destinationPath = '/admin-assets/company/';
                    $brochureName = rand().time().'.'.$brochure_file->getClientOriginalExtension();
                    $brochure_file->move(public_path().$destinationPath, $brochureName);
                    $brochure = $destinationPath.$brochureName;
                    $setting->brochure = $brochure;
                }else{
                    $brochure = $setting->brochure;
                    $setting->brochure = $brochure;
                }
                $setting->company_name =  $request->company_name;
                $setting->company_email =  $request->company_email;
                $setting->company_mobile =  $request->company_number;
                $setting->company_address =  $request->company_address;
                $setting->pan_number =  $request->pan_number;
                $setting->gst_no =  $request->gst_no;
                $setting->pin_code =  $request->pin_code;
                $setting->city =  $request->city;
                $setting->state =  $request->state;
                $setting->country =  $request->country;
                $setting->facebook_link =  $request->facebook_link;
                $setting->twitter_link =  $request->twitter_link;
                $setting->instagram_link =  $request->instagram_link;
                $setting->mission =  $request->mission;
                $setting->vision =  $request->vision;
                $setting->no_of_clients =  $request->number_of_client;
                $setting->no_of_products =  $request->number_of_products;
                $setting->no_of_machine =  $request->number_of_machine;
                $setting->year_of_experience =  $request->year_of_experience;
                $setting->save();
            }
            else{
                Setting::create([
                    'company_name' =>  $request->company_name,
                    'company_email'=>  $request->company_email,
                    'company_mobile'=>  $request->company_number,
                    'company_address'=>  $request->company_address,
                    'pan_number'=>  $request->pan_number,
                    'gst_no'=>  $request->gst_no,
                    'pin_code'=>  $request->pin_code,
                    'city'=>  $request->city,
                    'state'=>  $request->state,
                    'country'=>  $request->country
                ]);
            }
            return  redirect()->route('admin-settings')->with(['success' => 'Settings have been updated']);
        }
    }
}
