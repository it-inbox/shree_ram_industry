<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Activitylog\Models\Activity;

class UserController extends Controller
{
    public function index(){
        $users = User::orderBy('id','desc')->where('role_id',3)->paginate(10);
        return view('admin.users.index',['users' => $users]);
    }

    public function create(){
        $countries = Country::orderBy('id','asc')->get()->pluck('name','id')->toArray();
        return view('admin.users.create',['countries' => $countries]);
    }

    public function store(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make([
                'first_name' => 'required',
                'middle_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required'|'min:6'|'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
                'country' => 'required',
                'mobile' => 'required|max:10',
                'gender' => 'required',
                'date_of_birth' => 'required',
                'address' => 'required'
            ],[

            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $image = $request->file('profile_photo');
            $imageName = '';
            $destinationPath = '';
            if (isset($image)){
                $destinationPath = '/uploads/user/profile/';
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $image->move(public_path().$destinationPath, $imageName);
            }
            User::create([
                'username' => $request->username,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'country' => $request->country,
                'mobile'=> $request->mobile,
                'gender'=> ($request->gender == 1 ? User::MALE : User::FEMALE),
                'date_of_birth'=>$request->date_of_birth,
                'profile_photo' => $destinationPath.$imageName,
                'address' => $request->address,
                'status' => User::ACTIVE
            ]);
            $userActivity = Activity::all()->last();
            return  redirect()->route('user-list-view');
        }
    }

    public function edit($id){

        $user = User::where('id', $id)->first();

        $histories = Activity::where('subject_id',$id)->orderBy('id','desc')->get();
        return view('admin.users.edit',[
            'user' => $user,
            'histories' => $histories
        ]);
    }

    public function update(Request $request){
        $user = User::whereId($request->user_id)->first();

        if($request->isMethod('post')){
            $validator = Validator::make([
                'full_name' => 'required',
                'email' => 'required|email',
                'password' => 'required'|'min:6'|'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
                'phone' => 'required|max:10',
                'address' => 'required'
            ],[

            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }


            if($request->profile_photo != '' && $request->profile_photo != null){
                $image = $request->file('profile_photo');
                if (isset($image)){
                    $destinationPath = '/uploads/user/profile/';
                    $imageName = rand().time().'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().$destinationPath, $imageName);
                    $user->profile_photo = $destinationPath.$imageName;
                }
            }else{
                $imageName = $user->profile_photo;
                $user->profile_photo = $imageName;
            }
            $user->full_name = $request->full_name;
            $user->email = $request->email;
            $user->mobile = $request->phone;
            $user->password = Hash::make($request->password);
            $user->address = $request->address;
            $user->status = $request->status;
            $user->save();

            Activity::all()->last();
            return  redirect()->route('user-list-view');
        }
    }

    public function deleteUser($id){
        $user = User::whereId($id)->delete();
        if($user){
            return redirect()->route('user-list-view');
        }
    }
}
