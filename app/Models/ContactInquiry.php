<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactInquiry extends Model
{
    use HasFactory;

    const PENDING = 0, SEEN = 1;

    protected $fillable = [
        'name', 'contact', 'email', 'message', 'status'
    ];
}
