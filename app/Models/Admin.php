<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
    use HasFactory,SoftDeletes;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'username','email','password','status','last_logged_in_at','token'
    ];
}
