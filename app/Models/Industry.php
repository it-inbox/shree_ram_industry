<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    use HasFactory;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'slug','title','image','description','status'
    ];
}
