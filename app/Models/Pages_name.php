<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pages_name extends Model
{
    use HasFactory;

    protected $fillable = ['page_name', 'title', 'description', 'status'];

    public function setPageNameAttribute($value)
    {
        $this->attributes['page_name'] = ucfirst($value);
    }
    
}


